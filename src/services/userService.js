import axios from "axios";
import { BASE_URL } from "./configURL/configURL";

export const userService = {
  getLogin: (userForm) => {
    return axios({
      url: BASE_URL + "/auth/logIn",
      method: "POST",
      data: userForm,
    });
  },
  getUser: () => {
    return axios({
      url: BASE_URL + "/userList",
      method: "GET",
    });
  },
};
