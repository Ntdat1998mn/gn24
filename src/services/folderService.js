import axios from "axios";
import { BASE_URL } from "./configURL/configURL";

export const folderService = {
  getFolderList: (token) => {
    return axios({
      url: BASE_URL + "/folder",
      method: "GET",
      headers: { token },
    });
  },
};
