import { message } from "antd";

const showMessage = (content, type = "info") => {
  message[type](content);
};
const formatDateTime = (dateTimeString) => {
  const datePart = dateTimeString.split("T")[0];
  const timePart = dateTimeString.split("T")[1].split(".")[0];

  return `${datePart} ${timePart}`;
};

export { showMessage, formatDateTime };
