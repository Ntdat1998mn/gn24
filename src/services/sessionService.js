export const USERLOGIN_ERP = "USERLOGIN";

export const sessionService = {
  setUser: (user) => {
    let userJson = JSON.stringify(user);
    sessionStorage.setItem(USERLOGIN_ERP, userJson);
  },
  getUser: () => {
    let user = sessionStorage.getItem(USERLOGIN_ERP);
    if (user !== null) {
      return JSON.parse(user);
    } else {
      return null;
    }
  },
  remove: () => {
    sessionStorage.removeItem(USERLOGIN_ERP);
  },
};
