import "./App.css";

import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./HOC/Layout/Layout";
import HomePage from "./Pages/HomePage/HomePage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout Component={HomePage} />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
