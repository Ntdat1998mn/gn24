import React from "react";
import Header from "../../Components/Header/Header";
import Nav from "../../Components/Nav/Nav";

export default function Layout({ Component }) {
  return (
    <div>
      <Nav />
      <Header />
      <Component />
    </div>
  );
}
