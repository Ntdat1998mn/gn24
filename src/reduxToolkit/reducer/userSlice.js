import { createSlice } from "@reduxjs/toolkit";
import { sessionService } from "../../services/sessionService";

const initialState = {
  userInfor: sessionService.getUser(),
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUserInfor: (state, action) => {
      state.userInfor = { ...action.payload };
    },
  },
});

export const { setUserInfor } = userSlice.actions;

export default userSlice.reducer;
